// using JQuery for form validation
$(document).ready(function(){
    // validate form
    // set login variables for validation
    var lvl_username = "level"
    var lvl_password = "Access123"

    $("#password").focus(function(){

        // show password constraints when inputing password
        // then remove when focus is no longer on password input
        $("#passErrMsg").removeClass("noDisplay").addClass("display");

        // remove all error messages when re-typing password after invalid submit
        $('#messages').removeClass("noDisplay").addClass("display");
        $('#logErrMsg').removeClass("display").addClass("noDisplay");
        $('#emptyErrMsg').removeClass("display").addClass("noDisplay");
    }).focusout(function(){
        $("#passErrMsg").removeClass("display").addClass("noDisplay")
    });

    // remove all error messages when re-typing username after invalid submit
    $("#username").focus(function(){
        $('#messages').removeClass("noDisplay").addClass("display");
        $('#logErrMsg').removeClass("display").addClass("noDisplay");
        $('#emptyErrMsg').removeClass("display").addClass("noDisplay");
    })

    // validate form on submit
    $("#form").submit(function(e){
        
        // remvoe password constraints
        $("#passErrMsg").removeClass("display").addClass("noDisplay")

        // check for empty password or username
        if($("#username").val().length === 0 || $("#password").val().length === 0){

            // if either is empty display both error messages
            $('#messages').removeClass("noDisplay").addClass("display");
            $('#emptyErrMsg').removeClass("noDisplay").addClass("display");
            $('#logErrMsg').removeClass("noDisplay").addClass("display");

        }

        // check for invalid entries
        else if($("#username").val() != lvl_username || $("#password").val() != lvl_password){

            // if invalid remove empty error message and only display invalid entry message
            $('#messages').removeClass("noDisplay").addClass("display");
            $('#emptyErrMsg').removeClass("display").addClass("noDisplay");
            $('#logErrMsg').removeClass("noDisplay").addClass("display");

        }

        // check for successful entries
        if($("#username").val() === lvl_username && $("#password").val() === lvl_password){

            // if successful remove all error messages and form then display success message
            $('#messages').removeClass("noDisplay").addClass("display");
            $('#logErrMsg').removeClass("display").addClass("noDisplay");
            $('#emptyErrMsg').removeClass("display").addClass("noDisplay");
            $('#successMsg').removeClass("noDisplay").addClass("display");
            $('.loginForm').hide();

        }
        
        // prevent form from actually submiting and re-routing page
        e.preventDefault();
    })
})