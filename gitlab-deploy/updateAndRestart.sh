#!/bin/bash

# exit script on failure of any command
set -e

# Delete the old repo
rm -rf /home/ubuntu/var/www/level_test

# Move into directory where you will clone the new repo
cd /home/ubuntu/var/www
git clone https://gitlab.com/felipeo510.fs/level_test.git

# install nodejs on virtual machine
sudo apt-get install nodejs -g

# run npm install on project to add all dependencies
sudo npm install

# install pm2 to start the project
sudo npm install pm2 -g
pm2 start index.js