#!/bin/bash

# exit script at failure in any command
set -e

# set aws acces key
eval $(ssh-agent -s)
echo "$AWS_SECRET_ACCESS_KEY" | tr -d '\r' | ssh-add - > /dev/null

# run script to disable host key checking
./gitlab-deploy/disableHostKeyChecking.sh

# set varaible to hold ip address
# set array that will hold multiple ip address if needed
DEPLOY_SERVER=$DEPLOY_SERVER
SERVERS=(${DEPLOY_SERVER//,/ })
echo "SERVERS ${SERVERS}"

# iterate thru servers array and ssh into them
for server in "${SERVERS[@]}"
do
  echo "Deploying project on server ${server}"
  ssh ubuntu@${server} 'bash -s' < ./gitlab-deploy/updateAndRestart.sh
done